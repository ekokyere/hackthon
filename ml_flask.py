from flask import Flask
import quandl
import pickle as p
import numpy as np
import pandas as pd



app = Flask(__name__)



modelfile = ''

quandl.ApiConfig.api_key = "xi_GqGzd8iysF3zVunHu"

# Get data from various stocks for specific dates from quandl
aapl = quandl.get("EOD/AAPL", start_date="2014-01-01", end_date="2017-12-28")
msft = quandl.get("EOD/MSFT", start_date="2014-01-01", end_date="2017-12-28")

#select only the adj_close prices as our input , X
aapl = aapl[['Adj_Close']]
aapl["Prediction"] = aapl[["Adj_Close"]].shift(-1)
aapl.tail()
X = np.array(aapl.drop(["Prediction"],1))

#selects only first 10 prices for the model to predict from
test_input = X[:10]
print(test_input)

@app.route('/')
#testing the get request
def get():

    return "Hello World, i am working"

@app.route("/post")
#testing post request
def post():
      #retrieve predictions from model object
      predicton = (model.predict(test_input))

      prediction_json = pd.DataFrame(predicton).to_json(orient='split')
      return {"post": prediction_json}

if __name__ == '__main__':
    #retrieve the model pickle file
    model = p.load(open('models/lin_reg.pickle', 'rb'))
    app.run(debug = True)





